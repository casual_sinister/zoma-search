import React, { Component } from 'react';
import './App.css';
// import $ from 'jquery';

import Form from "./components/Form";
import Recipes from "./components/Recipes";

const API_KEY = "11170259d9dc01aa4d2b1398b5377fea";


class App extends Component {
	state = { // from react 16 onwards, we can set the state outside of constructor
		recipes: []

	}
	getRecipe = async (e) =>{
		const recipeName = e.target.elements.recipeName.value;

		e.preventDefault();
    let tempdata; 
    console.log("this is my name ok", recipeName, this);
    var url = `https://cors-anywhere.herokuapp.com/https://developers.zomato.com/api/v2.1/locations?query=${recipeName}&count=10`;
    await fetch(url, {
      method: 'GET', // or 'PUT'
      headers:{
        'Accept': 'application/json',
        'user-key': API_KEY
      }
    })
    .then((resp) => resp.json()) // Transform the data into json
  .then(function(data) {
    console.log("hello man", data.location_suggestions);
    tempdata = data.location_suggestions;
    // Create and append the li's to the ul
    })
  console.log("before doing it???", this);
   this.setState({recipes: tempdata});
   console.log("after it is done", this);

	}
  // componentDidMount = () => {
  //   const json = localStorage.getItem("recipes");
  //   const recipes = JSON.parse(json);
  //   this.setState({ recipes });
  // }
  componentDidUpdate = () => {
    const recipes = JSON.stringify(this.state.recipes);
    localStorage.setItem("recipes", recipes);
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Recipe Search</h1>
        </header>
        <Form getRecipe={this.getRecipe} />
        <Recipes recipes={this.state.recipes} />
        
      </div>
    );
  }
}

export default App;