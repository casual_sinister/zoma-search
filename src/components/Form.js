import React from 'react';
 
 // we will make a stateless functional component (and not a class based component) below, stateless cuz it needs no state

const Form = (props) =>(
<form onSubmit = {props.getRecipe} style = {{marginBottom: "2rem"}}>
   <input className = "form__input" type = "text" name= "recipeName"/>
   <button className = "form__button"> Search </button>
</form>
);

export default Form;